#pragma once

#include <QStringList>
#include <QAbstractListModel>

class EffectPoolModel : public QAbstractListModel {

  Q_OBJECT

public:
  EffectPoolModel();

  int rowCount(const QModelIndex &parent) const;
  QVariant data(const QModelIndex &index, int role) const;
  Qt::ItemFlags flags(const QModelIndex&) const;

private:
  QStringList _entries;
};
