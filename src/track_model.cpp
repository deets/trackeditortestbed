#include "track_model.h"


#define CLIP_TOP 0.0f
#define CLIP_HEIGHT 10.0f

TrackModel::TrackModel()
  : QObject(NULL)
{

}


void TrackModel::setup()
{
  {
    Clip c = { 0, 20, 10};
    _clips.push_back(c);
  }
  {
    Clip c = { 30, 40, 12};
    _clips.push_back(c);
  }
  for(int i=0; i < _clips.size(); ++i)
  {
    Q_EMIT clipAdded(_clips[i]);
  }
}


bool TrackModel::Clip::operator<(const Clip& other) const
{
  return start < other.start || length < other.length;
}


QRectF TrackModel::Clip::rect() const
{
  return QRectF(
      static_cast<qreal>(start),
      CLIP_TOP,
      static_cast<qreal>(length),
      CLIP_HEIGHT
  );
}
