/* Copyright 2016 - Diez Roggisch, Laseranimation Sollinger  -*- mode: c++; -*- */
#pragma once

#include <QtCore/QObject>
#include <QRect>

#include <vector>


class TrackModel : public QObject
{
  Q_OBJECT

public:
  typedef unsigned int tick;

  struct Clip {
    tick start;
    tick length;
    quint16 entry;

    QRectF rect() const;
    bool operator<(const Clip&) const;
  };

  TrackModel();
  void setup();

public Q_SLOTS:

Q_SIGNALS:

  void clipAdded(const TrackModel::Clip&);
  void clipRemoved(const TrackModel::Clip&);

private:

  std::vector<Clip> _clips;
};
