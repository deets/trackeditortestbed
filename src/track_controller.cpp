#include "track_controller.h"

#include <QDebug>

namespace detail {

EntryRect::EntryRect(const QRectF rect, const QPen& pen, const QBrush& brush)
  : QGraphicsRectItem(rect)
  , _brush(brush)
  , _entryName(new QGraphicsSimpleTextItem(this))
{
  setRect(QRectF(QPointF(0, 0), rect.size()));
  setPos(rect.topLeft());
  setPen(pen);
  setBrush(_brush);
  setAcceptHoverEvents(true);
  setAcceptDrops(true);
  setFlags(QGraphicsItem::ItemClipsChildrenToShape);
  _entryName->setFont(QFont("Helvetica", 4));
}


void EntryRect::hoverEnterEvent(QGraphicsSceneHoverEvent * event )
{
  setBrush(QBrush(QColor("red")));
}


void EntryRect::hoverLeaveEvent(QGraphicsSceneHoverEvent * event )
{
  setBrush(_brush);
}


void EntryRect::hoverMoveEvent(QGraphicsSceneHoverEvent * event )
{

}


void EntryRect::setEntryName(const QString& name)
{
  _entryName->setText(name);
  qDebug() << _entryName->boundingRect() << _entryName->pos();
}

}; // end ns detail


TrackController::TrackController(QGraphicsView& view, TrackModel& model)
  : _view(view)
  , _model(model)
{
  _view.setBackgroundBrush(QBrush(QColor("#4f8bd1")));
  _view.setScene(&_scene);

  QObject::connect(
      &_model, SIGNAL(clipAdded(const TrackModel::Clip&)),
      this, SLOT(clipAdded(const TrackModel::Clip&))
  );
  QObject::connect(
      &_model, SIGNAL(clipRemoved(const TrackModel::Clip&)),
      this, SLOT(clipRemoved(const TrackModel::Clip&))
  );
}


TrackController::~TrackController()
{
  _view.setScene(NULL);
}


void TrackController::clipAdded(const TrackModel::Clip& clip)
{
  Q_ASSERT(_items.count(clip) == 0);
  QPen pen(QColor("black"));
  pen.setCosmetic(true);
  detail::EntryRect* rect = new detail::EntryRect(
      clip.rect(),
      pen,
      entryBrush(clip.entry)
  );
  rect->setEntryName("Ein langer Effektname der alles ueberzieht");
  _scene.addItem(rect);
  _items[clip] = rect;
  _view.fitInView(_scene.itemsBoundingRect());
}


void TrackController::clipRemoved(const TrackModel::Clip& clip)
{
  Q_ASSERT(_items.count(clip) == 1);
}


QBrush TrackController::entryBrush(quint16)
{
  return QBrush(QColor("#276172"));
}
