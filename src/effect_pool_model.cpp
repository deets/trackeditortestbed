#include "effect_pool_model.h"


EffectPoolModel::EffectPoolModel()
  : QAbstractListModel(NULL)
{
  _entries << "ein" << "netter" << "effect";
}


int EffectPoolModel::rowCount(const QModelIndex &parent) const
{
  return static_cast<int>(_entries.size());
}


QVariant EffectPoolModel::data(const QModelIndex &index, int role) const
{
  if(role == Qt::DisplayRole)
  {
    return _entries[index.row()];
  }
  return QVariant();
}


Qt::ItemFlags EffectPoolModel::flags(const QModelIndex&) const
{
  return Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsEnabled;
}
