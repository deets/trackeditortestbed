/* Copyright 2016 - Diez Roggisch, Laseranimation Sollinger  -*- mode: c++; -*- */

#include <QtGui/QGraphicsView>

class TrackView : public QGraphicsView
{
  Q_OBJECT

public:
  TrackView(QWidget*);

public Q_SLOTS:

Q_SIGNALS:

protected:
  void dragEnterEvent(QDragEnterEvent * event);
  void dragLeaveEvent(QDragLeaveEvent * event);
  void dragMoveEvent(QDragMoveEvent * event);
private:

};
