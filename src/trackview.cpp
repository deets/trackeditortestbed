/* Copyright 2016 - Diez Roggisch, Laseranimation Sollinger  -*- mode: c++; -*- */


#include "trackview.h"

#include <QDebug>


TrackView::TrackView(QWidget* parent) : QGraphicsView(parent)
{
   setAcceptDrops(true);
}


void TrackView::dragEnterEvent(QDragEnterEvent * event)
{
  qDebug() << "dragEnterEvent" << event;
  QGraphicsView::dragEnterEvent(event);
}


void TrackView::dragLeaveEvent(QDragLeaveEvent * event)
{
  qDebug() << "dragLeaveEvent" << event;
  QGraphicsView::dragLeaveEvent(event);
}


void TrackView::dragMoveEvent(QDragMoveEvent * event)
{
  qDebug() << "dragMoveEvent" << event;
  QGraphicsView::dragMoveEvent(event);
}
