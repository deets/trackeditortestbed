/* Copyright 2016 - Diez Roggisch, Laseranimation Sollinger  -*- mode: c++; -*- */
#include "track_model.h"

#include <map>

#include <QtCore/QObject>
#include <QGraphicsView>
#include <QGraphicsRectItem>


namespace detail {

class EntryRect : public QGraphicsRectItem
{

public:
  EntryRect(const QRectF rect, const QPen& pen, const QBrush& brush);

  void setEntryName(const QString&);

  virtual void	hoverEnterEvent ( QGraphicsSceneHoverEvent * event );
  virtual void	hoverLeaveEvent ( QGraphicsSceneHoverEvent * event );
  virtual void	hoverMoveEvent ( QGraphicsSceneHoverEvent * event );

public Q_SLOTS:

Q_SIGNALS:

private:
  QBrush _brush;
  QGraphicsSimpleTextItem* _entryName;
};

} // end ns detail



class TrackController : public QObject
{
  Q_OBJECT

public:
  TrackController(QGraphicsView&, TrackModel&);
  ~TrackController();

public Q_SLOTS:

  void clipAdded(const TrackModel::Clip&);
  void clipRemoved(const TrackModel::Clip&);

Q_SIGNALS:

private:

  QBrush entryBrush(quint16 entry);

  QGraphicsView& _view;
  TrackModel& _model;
  QGraphicsScene _scene;

  std::map<TrackModel::Clip, QGraphicsItem*> _items;

  //  std::map<quint16, QBrush> _entryBrushes;
};
