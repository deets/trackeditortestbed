#include "ui_mainwindow.h"
#include "effect_pool_model.h"
#include "track_controller.h"

#include <QApplication>
#include <QMainWindow>
#include <QListView>
#include <QGraphicsView>


QGraphicsView* setupMainWindow(QMainWindow& mainWindow, EffectPoolModel* effectPoolModel)
{
  Ui_MainWindow uiMainWindow;
  uiMainWindow.setupUi(&mainWindow);
  QListView* effectPool = mainWindow.findChild<QListView*>("effectPool");
  Q_ASSERT(effectPool);
  effectPool->setModel(effectPoolModel);
  QGraphicsView* trackView = mainWindow.findChild<QGraphicsView*>("trackView");
  Q_ASSERT(trackView);
  return trackView;
}


int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    EffectPoolModel effectPool;
    QMainWindow mainWindow;
    TrackModel trackModel;

    TrackController controller(
        *setupMainWindow(mainWindow, &effectPool),
        trackModel
    );
    mainWindow.show();
    trackModel.setup();
    return app.exec();
}
