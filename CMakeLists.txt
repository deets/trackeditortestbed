# generate for windows VS2008:
# cmake -G "Visual Studio 9 2008" ..
cmake_minimum_required(VERSION 3.5)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(
     Qt4 4.8 REQUIRED
     QtNetwork
     QtTest
     QtXml
     QtGui
)

include_directories(
   src
)

qt4_wrap_ui(mainwindow_ui src/mainwindow.ui)

add_executable(
    TrackEditorTestbed MACOSX_BUNDLE
    src/main.cpp
    src/effect_pool_model.cpp
    src/track_controller.h
    src/track_controller.cpp
    src/track_model.h
    src/track_model.cpp
    src/trackview.h
    src/trackview.cpp
    ${mainwindow_ui}
)

target_link_libraries(
   TrackEditorTestbed
   Qt4::QtNetwork
   Qt4::QtXml
   Qt4::QtGui
   Qt4::phonon
)   


set_target_properties(
  TrackEditorTestbed
  PROPERTIES
  MACOSX_BUNDLE_GUI_IDENTIFIER de.roggisch.TrackEditorTestbed
)
